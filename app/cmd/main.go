package main

import (
	tea "github.com/charmbracelet/bubbletea"
	"log"
	"thardians/pkg/titlescreen"
)

func main() {
	p := tea.NewProgram(titlescreen.NewModel(true),
		tea.WithAltScreen(),
		tea.WithMouseCellMotion(),
	)
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
