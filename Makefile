.PHONY: create-demo
create-demo: build
	vhs ./readme/demo.tape

.PHONY: build
build:
	go build -o bin/thardians ./app/cmd

.PHONY: build-ssh
build-ssh:
	go build -o bin/thardians-ssh ./app/ssh

.PHONY: run
run: build
	./bin/thardians

.PHONY: run-ssh
run: build
	./bin/thardians-ssh

.PHONY: test
test:
	go test -v ./...
