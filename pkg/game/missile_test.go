package game

import (
	"reflect"
	"testing"
)

func TestMoveMissiles(t *testing.T) {
	tests := []struct {
		name           string
		missiles       Missiles
		limit          int
		expectedOutput Missiles
	}{
		{
			"FriendlyMissileWithinGrid",
			Missiles{
				NewMissile(true, 5, 1),
			},
			10,
			Missiles{
				NewMissile(true, 4, 1),
			},
		},
		{
			"FriendlyMissileOutOfBounds",
			Missiles{
				NewMissile(true, 0, 2),
			},
			10,
			Missiles{},
		},
		{
			"NonFriendlyMissileWithinGrid",
			Missiles{
				NewMissile(false, 5, 1),
			},
			10,
			Missiles{
				NewMissile(false, 6, 1),
			},
		},
		{
			"NonFriendlyMissileOutOfBounds",
			Missiles{
				NewMissile(false, 10, 1),
			},
			10,
			Missiles{},
		},
		{
			"MultipleMixedMissiles",
			Missiles{
				NewMissile(true, 5, 1),
				NewMissile(false, 1, 3),
			},
			10,
			Missiles{
				NewMissile(true, 4, 1),
				NewMissile(false, 2, 3),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MoveMissiles(tt.missiles, tt.limit); !reflect.DeepEqual(got, tt.expectedOutput) {
				t.Errorf("MoveMissiles() = %v, want %v", got, tt.expectedOutput)
			}
		})
	}
}

func TestMissileDrawInGrid(t *testing.T) {
	grid := blankGrid()
	missile0 := NewMissile(true, 0, 0)
	missile1 := NewMissile(false, 1, 1)

	tests := []struct {
		name     string
		missiles Missiles
		want     Grid
	}{
		{
			name:     "NoMissiles",
			missiles: Missiles{},
			want:     blankGrid(),
		},
		{
			name:     "SingleMissile",
			missiles: Missiles{missile0},
			want: func() Grid {
				grid := blankGrid()
				grid[0][0].Missile = &CellItem[Missile]{Item: &missile0, RenderChar: "|"}
				return grid
			}(),
		},
		{
			name:     "MultipleMissiles",
			missiles: Missiles{missile0, missile1},
			want: func() Grid {
				grid := blankGrid()
				grid[0][0].Missile = &CellItem[Missile]{Item: &missile0, RenderChar: "|"}
				grid[1][1].Missile = &CellItem[Missile]{Item: &missile1, RenderChar: "|"}
				return grid
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			grid.ClearAll()
			tt.missiles.DrawInGrid(grid)
			if !compareGrids(grid, tt.want) {
				t.Errorf("Got %v, want %v", grid, tt.want)
			}
		})
	}
}

func TestCountFriendly(t *testing.T) {
	tests := []struct {
		name  string
		input Missiles
		want  int
	}{
		{
			name:  "No missiles",
			input: Missiles{},
			want:  0,
		},
		{
			name: "All friendly missiles",
			input: Missiles{
				NewMissile(true, 0, 0),
				NewMissile(true, 1, 1),
			},
			want: 2,
		},
		{
			name: "All enemy missiles",
			input: Missiles{
				NewMissile(false, 0, 0),
				NewMissile(false, 1, 1),
			},
			want: 0,
		},
		{
			name: "Mixed friendly and enemy missiles",
			input: Missiles{
				NewMissile(true, 0, 0),
				NewMissile(false, 1, 1),
			},
			want: 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.input.CountFriendly(); got != tt.want {
				t.Errorf("CountFriendly() = %d, want %d", got, tt.want)
			}
		})
	}
}
