package game

import "sync"

// Level contains difficulty parameters: how fast the invaders are, how often they shoot and their initial health points.
type Level struct {
	InvadersUpdateEvery     int
	InvaderShootProbability int
	ShipInitialHealth       int
	PawnInitialHealth       int
}

type Levels []Level

var instantiatedLevels Levels
var once sync.Once

// GetLevels retrieves a list of levels and their values
func GetLevels() Levels {
	once.Do(func() {
		instantiatedLevels = Levels{
			{
				InvadersUpdateEvery:     5,
				InvaderShootProbability: 5,
				ShipInitialHealth:       2,
				PawnInitialHealth:       1,
			},
			{
				InvadersUpdateEvery:     5,
				InvaderShootProbability: 4,
				ShipInitialHealth:       2,
				PawnInitialHealth:       1,
			},
			{
				InvadersUpdateEvery:     4,
				InvaderShootProbability: 4,
				ShipInitialHealth:       2,
				PawnInitialHealth:       1,
			},
			{
				InvadersUpdateEvery:     3,
				InvaderShootProbability: 3,
				ShipInitialHealth:       3,
				PawnInitialHealth:       1,
			},
			{
				InvadersUpdateEvery:     3,
				InvaderShootProbability: 2,
				ShipInitialHealth:       3,
				PawnInitialHealth:       2,
			},
			{
				InvadersUpdateEvery:     2,
				InvaderShootProbability: 2,
				ShipInitialHealth:       4,
				PawnInitialHealth:       2,
			},
			{
				InvadersUpdateEvery:     1,
				InvaderShootProbability: 1,
				ShipInitialHealth:       4,
				PawnInitialHealth:       3,
			},
			{
				InvadersUpdateEvery:     1,
				InvaderShootProbability: 1,
				ShipInitialHealth:       5,
				PawnInitialHealth:       4,
			},
		}

	})
	return instantiatedLevels
}

// GetLevel returns the level details for the supplied level number (starting at 1).
func GetLevel(levelNum int) Level {
	levels := GetLevels()
	if levelNum > 0 {
		levelNum--
	}
	if levelNum < len(levels) {
		return levels[levelNum]
	}
	return levels[len(levels)-1]
}
