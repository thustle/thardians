package game

import (
	"testing"
)

func TestRenderString(t *testing.T) {
	missile := Missile{}
	invader := Invader{}
	shield := Shield{}
	defender := Defender{}
	// Test cases
	tests := []struct {
		name    string
		cell    *Cell
		wantRes string
	}{
		{
			name:    "Empty Cell",
			cell:    &Cell{},
			wantRes: " ",
		},
		{
			name:    "Cell with Missile",
			cell:    &Cell{Missile: &CellItem[Missile]{&missile, "M"}},
			wantRes: "M",
		},
		{
			name:    "Cell with Invader",
			cell:    &Cell{Invader: &CellItem[Invader]{&invader, "I"}},
			wantRes: "I",
		},
		{
			name:    "Cell with Shield",
			cell:    &Cell{Shield: &CellItem[Shield]{&shield, "S"}},
			wantRes: "S",
		},
		{
			name:    "Cell with Defender",
			cell:    &Cell{Defender: &CellItem[Defender]{&defender, "D"}},
			wantRes: "D",
		},
		{
			name:    "Cell with multiple items, missile takes precedence",
			cell:    &Cell{Missile: &CellItem[Missile]{&missile, "M"}, Invader: &CellItem[Invader]{&invader, "I"}},
			wantRes: "M",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if result := test.cell.RenderString(); result != test.wantRes {
				t.Errorf("Expected '%s', got '%s'", test.wantRes, result)
			}
		})
	}
}
