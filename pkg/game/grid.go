package game

// Grid is a representation of the game screen containing cells which hold the char to draw and the items occupying that grid position.
type Grid [][]Cell

func (g *Grid) ClearAll() {
	for i := range *g {
		for j := range (*g)[i] {
			(*g)[i][j].Clear()
		}
	}
}

func (g *Grid) ClearRows(rows ...int) {
	for _, i := range rows {
		if i >= 0 && i < len(*g) {
			for j := range (*g)[i] {
				(*g)[i][j].Clear()
			}
		}
	}
}
