package game

import (
	"reflect"
	"testing"
)

func TestDoMissilesWithShield(t *testing.T) {
	shield := Shield{X: 0, Y: 0}
	hitShield := Shield{X: 0, Y: 1}
	hitMissile := Missile{X: 0, Y: 1}
	missile := Missile{X: 0, Y: 2}
	grid := [][]Cell{{
		{Shield: &CellItem[Shield]{Item: &shield}, Missile: nil},
		{Shield: &CellItem[Shield]{Item: &hitShield}, Missile: &CellItem[Missile]{Item: &hitMissile}},
		{Shield: nil, Missile: &CellItem[Missile]{Item: &missile}},
	}}
	expectedShieldLeft := Shields{&shield}
	expectedMissilesLeft := Missiles{missile}

	model := Model{
		shields:  Shields{&shield, &hitShield},
		missiles: Missiles{missile, hitMissile},
		grid:     grid,
	}

	for i := range model.grid {
		for j := range model.grid[i] {
			doMissilesWithShield(&model, i, j)
		}
	}

	if !reflect.DeepEqual(model.missiles, expectedMissilesLeft) {
		t.Errorf("doMissilesWithShield = %v, want %v", model, expectedMissilesLeft)
	}
	if !reflect.DeepEqual(model.shields, expectedShieldLeft) {
		t.Errorf("doMissilesWithShield = %v, want %v", model, expectedShieldLeft)
	}
}

func TestDoMissilesWithDefender(t *testing.T) {
	defender := Defender{X: 0, Y: 0}
	missile := Missile{X: 0, Y: 1}
	cases := []struct {
		name     string
		step     int
		expected bool
		grid     [][]Cell
	}{
		{
			name:     "Defender hit with missile",
			step:     1,
			expected: true,
			grid: [][]Cell{{
				{Defender: &CellItem[Defender]{Item: &defender}, Missile: nil},
				{Defender: &CellItem[Defender]{Item: &defender}, Missile: &CellItem[Missile]{Item: &missile}},
			}},
		},
		{
			name:     "Defender not hit with missile",
			step:     2,
			expected: false,
			grid: [][]Cell{{
				{Defender: &CellItem[Defender]{Item: &defender}, Missile: &CellItem[Missile]{Item: &missile}},
				{Defender: &CellItem[Defender]{Item: &defender}, Missile: nil},
			}},
		},
		{
			name:     "No defender",
			step:     3,
			expected: false,
			grid: [][]Cell{{
				{Defender: nil, Missile: nil},
				{Defender: nil, Missile: &CellItem[Missile]{Item: &missile}},
			}},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			m := &Model{
				grid: c.grid,
			}

			got := doMissilesWithDefender(m, 0, 1)

			if got != c.expected {
				t.Errorf("doMissilesWithDefender() = %v, want %v", got, c.expected)
			}
		})
	}
}
