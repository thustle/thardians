package game

import (
	"reflect"
	"testing"
)

func TestGrid_ClearRows(t *testing.T) {
	var tests = []struct {
		name string
		grid Grid
		rows []int
		want Grid
	}{
		{
			name: "Empty Grid ",
			grid: Grid{},
			rows: []int{0},
			want: Grid{},
		},
		{
			name: "Single row Grid",
			grid: Grid{
				[]Cell{{Invader: &CellItem[Invader]{}}},
			},
			rows: []int{0},
			want: Grid{{Cell{}}},
		},
		{
			name: "Multiple rows not selected",
			grid: Grid{
				[]Cell{{Invader: &CellItem[Invader]{}}},
				[]Cell{{Missile: &CellItem[Missile]{}}},
			},
			rows: []int{},
			want: Grid{
				[]Cell{{Invader: &CellItem[Invader]{}}},
				[]Cell{{Missile: &CellItem[Missile]{}}},
			},
		},
		{
			name: "Multiple rows partially selected",
			grid: Grid{
				[]Cell{{Invader: &CellItem[Invader]{}}},
				[]Cell{{Missile: &CellItem[Missile]{}}},
				[]Cell{{Shield: &CellItem[Shield]{}}},
				[]Cell{{Shield: &CellItem[Shield]{}}},
			},
			rows: []int{2, 3},
			want: Grid{
				[]Cell{{Invader: &CellItem[Invader]{}}},
				[]Cell{{Missile: &CellItem[Missile]{}}},
				[]Cell{{}},
				[]Cell{{}},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.grid.ClearRows(tt.rows...)
			if !reflect.DeepEqual(tt.grid, tt.want) {
				t.Fatalf("expected: %v, got: %v", tt.want, tt.grid)
			}
		})
	}
}

func blankGridWithXY(x, y int) Grid {
	grid := make([][]Cell, x)
	for i := range grid {
		grid[i] = make([]Cell, y)
	}
	return grid
}

func blankGrid() Grid {
	return Grid{
		{Cell{}, Cell{}, Cell{}},
		{Cell{}, Cell{}, Cell{}},
	}
}

type gridItem interface {
	getXY() (x, y int)
}

func (d *Defender) getXY() (x, y int) { return d.X, d.Y }
func (m Missile) getXY() (x, y int)   { return m.X, m.Y }
func (s Shield) getXY() (x, y int)    { return s.X, s.Y }
func (i Invader) getXY() (x, y int)   { return i.X, i.Y }

func compareCells(cellA Cell, cellB Cell) bool {
	return compareCell(cellA.Missile, cellB.Missile) && (cellsNil(cellA.Missile, cellB.Missile) || compareCellItem(cellA.Missile.Item, cellB.Missile.Item)) &&
		compareCell(cellA.Defender, cellB.Defender) && (cellsNil(cellA.Defender, cellB.Defender) || compareCellItem(cellA.Defender.Item, cellB.Defender.Item)) &&
		compareCell(cellA.Shield, cellB.Shield) && (cellsNil(cellA.Shield, cellB.Shield) || compareCellItem(cellA.Shield.Item, cellB.Shield.Item)) &&
		compareCell(cellA.Invader, cellB.Invader) && (cellsNil(cellA.Invader, cellB.Invader) || compareCellItem(cellA.Invader.Item, cellB.Invader.Item))
}

func compareCellItem[T gridItem](itemA, itemB T) bool {
	ax, ay := itemA.getXY()
	bx, by := itemB.getXY()
	if ax != bx {
		return false
	}
	if ay != by {
		return false
	}
	return true
}

func cellsNil[T any](cellItemA *CellItem[T], cellItemB *CellItem[T]) bool {
	if cellItemA == nil && cellItemB == nil {
		return true
	}
	return false
}

func compareCell[T any](cellItemA *CellItem[T], cellItemB *CellItem[T]) bool {
	if cellsNil(cellItemA, cellItemB) {
		return true
	}
	if cellItemA == nil || cellItemB == nil {
		return false
	}
	if cellItemA.RenderChar != cellItemB.RenderChar {
		return false
	}
	return true
}

func compareGrids(a, b Grid) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if len(a[i]) != len(b[i]) {
			return false
		}
		for j := range a[i] {
			if !compareCells(a[i][j], b[i][j]) {
				return false
			}
		}
	}
	return true
}
