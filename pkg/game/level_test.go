package game

import "testing"

func TestGetLevel(t *testing.T) {
	tests := []struct {
		name      string
		levelNum  int
		wantLevel Level
	}{
		{name: "Test_level_0", levelNum: 0, wantLevel: GetLevels()[0]},
		{name: "Test_level_inside_range", levelNum: 1, wantLevel: GetLevels()[0]},
		{name: "Test_level_7_range", levelNum: 7, wantLevel: GetLevels()[6]},
		{name: "Test_level_8_range", levelNum: len(GetLevels()), wantLevel: GetLevels()[len(GetLevels())-1]},
		{name: "Test_level_outside_range", levelNum: 100, wantLevel: GetLevels()[len(GetLevels())-1]},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotLevel := GetLevel(tt.levelNum); gotLevel != tt.wantLevel {
				t.Errorf("GetLevel() = %v, want %v", gotLevel, tt.wantLevel)
			}
		})
	}
}
