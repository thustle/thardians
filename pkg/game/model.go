package game

import (
	"fmt"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"strings"
	"thardians/pkg/styles"
	"time"
)

type direction int

const (
	dirLeft direction = iota
	dirRight
)

const (
	gridHeight = 22
	gridWidth  = 78
)

// Model is the Bubble Tea model of the game.
type Model struct {
	titleScreen   tea.Model
	style         lipgloss.Style
	width, height int
	invaders      Invaders
	defender      Defender
	shields       Shields
	missiles      Missiles
	dir           direction
	grid          Grid
	score         int
	level         int
	lives         int
	paused        bool
	transitioning bool
}

// NewModel creates a new model to begin a new game.
func NewModel(titleScreen tea.Model, style lipgloss.Style, width, height int) Model {
	level := 1
	grid := make([][]Cell, gridHeight) // 0-20 invaders, 16-19 Shields, 20-21 defence
	for i := range grid {
		grid[i] = make([]Cell, gridWidth)
	}
	m := Model{
		titleScreen: titleScreen,
		style:       style,
		width:       width,
		height:      height,
		dir:         dirRight,
		invaders:    SetupInvaders(GetLevel(level)),
		defender:    NewDefender(20, 38),
		shields:     SetupShields(),
		grid:        grid,
		lives:       3,
		level:       level,
	}
	return m
}

// EndGame is a message to end the current game
type EndGame struct {
	Points int
}

func gameOver(points int) tea.Cmd {
	return tea.Tick(time.Millisecond*10, func(t time.Time) tea.Msg {
		return EndGame{points}
	})
}

// StartGameMessage is a message to start a game
type StartGameMessage struct{}

func StartGame() tea.Cmd {
	return tea.Tick(time.Millisecond*100, func(t time.Time) tea.Msg {
		return StartGameMessage{}
	})
}

// UpdateStateMessage is the message sent when a change in game state is made (e.g. lost a life, level complete)
type UpdateStateMessage struct {
	state ResultState
}

func transitionState(state ResultState) tea.Cmd {
	return tea.Tick(time.Millisecond*3000, func(t time.Time) tea.Msg {
		return UpdateStateMessage{state}
	})
}

type updateTickMsg time.Time

func updateTickCommand() tea.Cmd {
	return tea.Tick(time.Millisecond*100, func(t time.Time) tea.Msg {
		return updateTickMsg(t)
	})
}

func (m Model) hasPlayableSize() bool {
	return m.height >= 25 && m.width >= styles.MaxWidth
}

// Init is run once when the program starts
func (m Model) Init() tea.Cmd {
	return nil
}

// Update handles all game state changes.
func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	commands := make([]tea.Cmd, 0)
	switch msg := msg.(type) {
	case StartGameMessage: // New game is being started - start the update ticker.
		return m, updateTickCommand()
	case UpdateStateMessage: // Status update. Defender hit, level complete, or invader reached bottom.
		switch msg.state {
		case ResultStateHit:
			m.lives--
			if m.lives <= 0 {
				return m.titleScreen, gameOver(m.score)
			}
			m.defender = NewDefender(20, 38)
			m.missiles = Missiles{}
		case ResultStateLevelComplete:
			m.level++
			m.dir = dirRight
			m.invaders = SetupInvaders(GetLevel(m.level))
			m.defender = NewDefender(20, 38)
			m.shields = SetupShields()
			m.missiles = Missiles{}
		case ResultStateDeadRestart:
			m.lives--
			if m.lives <= 0 {
				return m.titleScreen, gameOver(m.score)
			}
			m.dir = dirRight
			m.invaders = SetupInvaders(GetLevel(m.level))
			m.defender = NewDefender(20, 38)
			m.shields = SetupShields()
			m.missiles = Missiles{}
		default:
		}
		m.transitioning = false
		return m, updateTickCommand()
	case tea.WindowSizeMsg: // Window size has changed. If screen is too small then stop the update ticker to pause the game.
		m.width = min(styles.MaxWidth, msg.Width)
		m.height = min(styles.MaxHeight, msg.Height)
		if !m.paused && m.hasPlayableSize() {
			commands = append(commands, updateTickCommand())
		}
		return m, tea.Batch(commands...)
	case updateTickMsg: // Update ticker. Update the invaders and missiles, do the collision detection and update the status of the items.
		if !m.hasPlayableSize() {
			return m, nil
		}
		m.grid.ClearAll()
		updateInvaders := m.invaders.Tick()
		if updateInvaders {
			switch m.dir {
			case dirRight:
				rightmostInvaderChar := m.invaders.RightmostPos()
				if rightmostInvaderChar < len(m.grid[0])-1 {
					m.invaders.NextCol(1)
				} else {
					m.dir = dirLeft
					m.invaders.NextRow()
				}
			case dirLeft:
				leftmostInvaderChar := m.invaders.LeftmostPos()
				if leftmostInvaderChar > 0 {
					m.invaders.NextCol(-1)
				} else {
					m.dir = dirRight
					m.invaders.NextRow()
				}
			}
		}
		m.invaders.DrawInGrid(m.grid)
		m.defender.DrawInGrid(m.grid)
		m.shields.DrawInGrid(m.grid)
		m.missiles = MoveMissiles(m.missiles, len(m.grid)-1)

		newMissile := m.invaders.DecideOnShoot()
		if newMissile != nil {
			m.missiles = append(m.missiles, *newMissile)
		}
		m.missiles.DrawInGrid(m.grid)

		result := collisionDetection(&m)
		m.score += result.Points
		if result.State != ResultStateOK { // Game state has changed (e.g. player dead). Mark game as transitioning to prevent new input and call a state change.
			m.defender.DrawInGrid(m.grid)
			m.transitioning = true
			return m, transitionState(result.State)
		}

		if !m.paused {
			commands = append(commands, updateTickCommand())
		}
		return m, tea.Batch(commands...)
	case tea.KeyMsg: // Handle player input.
		switch msg.String() {
		case "esc":
			return m.titleScreen, nil
		case "left":
			if !m.paused && !m.transitioning {
				m.grid.ClearRows(20, 21)
				m.defender.Move(-1, len(m.grid[0])-1)
				m.defender.DrawInGrid(m.grid)
			}
		case "right":
			if !m.paused && !m.transitioning {
				m.grid.ClearRows(20, 21)
				m.defender.Move(1, len(m.grid[0])-1)
				m.defender.DrawInGrid(m.grid)
			}
		case " ":
			if !m.paused && !m.transitioning {
				friendlyMissiles := m.missiles.CountFriendly()
				if friendlyMissiles < 4 { // Max 4 friendly missiles at once to prevent spacebar spamming!
					newMissile := NewMissile(true, 19, m.defender.Y+1)
					hitInvaderOnAdd := m.invaders.HitInGrid(newMissile.X, newMissile.Y) // Check new missile hasn't immediately hit something.
					if !hitInvaderOnAdd {
						m.missiles = append(m.missiles, newMissile)
						newMissile.DrawInGrid(m.grid)
					}
				}
			}
		case "ctrl+z":
			m.paused = true
			return m, tea.Suspend
		case "p": // Pause/Unpause the game if game is in playable state.
			m.paused = !m.paused
			if !m.paused && !m.transitioning && m.hasPlayableSize() {
				return m, updateTickCommand()
			}
		}
	}
	return m, nil
}

// View displays the current game state
func (m Model) View() string {
	if !m.hasPlayableSize() {
		return styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
			"Please increase your term size to at least 80x25",
		)
	}
	titleText := "Thardians!"
	if m.paused {
		titleText = titleText + " (paused)"
	}
	topPanel := lipgloss.JoinHorizontal(lipgloss.Left,
		m.style.Render(fmt.Sprintf("Level: %d  Lives: %d", m.level, m.lives)),
		styles.TitleStyle.Render(titleText),
		strings.Repeat(" ", 21-len(titleText)),
		styles.ScoreStyle.Render(fmt.Sprintf("Score: %d", m.score)))
	builder := strings.Builder{}
	builder.Grow((gridHeight * (gridWidth + 1)) - 1)
	for i, row := range m.grid {
		if i > 0 {
			builder.WriteString("\n")
		}
		for _, item := range row {
			builder.WriteString(item.RenderString())
		}
	}
	middlePanel := m.style.Render(builder.String())
	body := lipgloss.JoinVertical(lipgloss.Top, topPanel, middlePanel)
	panel := styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
		body,
	)
	return panel
}
