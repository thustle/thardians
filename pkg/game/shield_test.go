package game

import (
	"testing"
)

func TestSetupShields(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "Setup_Shields",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			shields := SetupShields()

			wanted := 3 * 14
			if wanted != len(shields) {
				t.Errorf("SetupShields() length = %d, want %d", len(shields), wanted)
			}
		})
	}
}

func TestDrawInGrid(t *testing.T) {
	singleShield := make([]*Shield, 0)
	singleShield = append(singleShield, NewShield(0, 0)...)

	testCases := []struct {
		name    string
		shields Shields
		want    Grid
	}{
		{
			name:    "NoShields",
			shields: Shields{},
			want:    blankGridWithXY(3, 7),
		},
		{
			name: "SingleShield",
			shields: func() Shields {
				s := make([]*Shield, 0)
				s = append(s, NewShield(0, 0)...)
				return s
			}(),
			want: func() Grid {
				grid := blankGridWithXY(3, 7)
				grid[0][2].Shield = &CellItem[Shield]{Item: singleShield[0], RenderChar: "█"}
				grid[0][3].Shield = &CellItem[Shield]{Item: singleShield[1], RenderChar: "█"}
				grid[0][4].Shield = &CellItem[Shield]{Item: singleShield[2], RenderChar: "█"}
				grid[1][1].Shield = &CellItem[Shield]{Item: singleShield[3], RenderChar: "█"}
				grid[1][2].Shield = &CellItem[Shield]{Item: singleShield[4], RenderChar: "█"}
				grid[1][3].Shield = &CellItem[Shield]{Item: singleShield[5], RenderChar: "█"}
				grid[1][4].Shield = &CellItem[Shield]{Item: singleShield[6], RenderChar: "█"}
				grid[1][5].Shield = &CellItem[Shield]{Item: singleShield[7], RenderChar: "█"}
				grid[2][0].Shield = &CellItem[Shield]{Item: singleShield[8], RenderChar: "█"}
				grid[2][1].Shield = &CellItem[Shield]{Item: singleShield[9], RenderChar: "█"}
				grid[2][2].Shield = &CellItem[Shield]{Item: singleShield[10], RenderChar: "█"}
				grid[2][4].Shield = &CellItem[Shield]{Item: singleShield[11], RenderChar: "█"}
				grid[2][5].Shield = &CellItem[Shield]{Item: singleShield[12], RenderChar: "█"}
				grid[2][6].Shield = &CellItem[Shield]{Item: singleShield[13], RenderChar: "█"}
				return grid
			}(),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			grid := blankGridWithXY(3, 7)

			tc.shields.DrawInGrid(grid)
			if !compareGrids(grid, tc.want) {
				t.Errorf("Got %v, want %v", grid, tc.want)
			}
		})
	}
}
