package game

import (
	"math"
	"math/rand/v2"
	"slices"
	"thardians/pkg/styles"
)

type Invaders struct {
	Invaders         []*Invader
	Cycle            int
	UpdateEvery      int
	MaxUpdateEvery   int
	ShootProbability int
}

func SetupInvaders(lvl Level) Invaders {
	invaders := make([]*Invader, 0, 48)
	invaders = append(invaders, NewShip(0, 0, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 4, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 8, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 12, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 16, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 20, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 24, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 28, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 32, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 36, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 40, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(0, 44, lvl.ShipInitialHealth))

	invaders = append(invaders, NewShip(3, 0, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 4, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 8, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 12, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 16, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 20, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 24, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 28, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 32, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 36, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 40, lvl.ShipInitialHealth))
	invaders = append(invaders, NewShip(3, 44, lvl.ShipInitialHealth))

	invaders = append(invaders, NewPawn(6, 0, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 4, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 8, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 12, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 16, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 20, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 24, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 28, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 32, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 36, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 40, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(6, 44, lvl.PawnInitialHealth))

	invaders = append(invaders, NewPawn(8, 0, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 4, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 8, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 12, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 16, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 20, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 24, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 28, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 32, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 36, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 40, lvl.PawnInitialHealth))
	invaders = append(invaders, NewPawn(8, 44, lvl.PawnInitialHealth))

	return Invaders{
		Invaders:         invaders,
		Cycle:            0,
		UpdateEvery:      lvl.InvadersUpdateEvery,
		MaxUpdateEvery:   lvl.InvadersUpdateEvery,
		ShootProbability: lvl.InvaderShootProbability,
	}
}

// Tick is to be triggered on each frame tick of the game. It will decide whether the aliens should move during this cycle
// (slower aliens only move after more ticks have cycled).
func (inv *Invaders) Tick() bool {
	inv.Cycle++
	if inv.Cycle >= min(inv.UpdateEvery, inv.MaxUpdateEvery) {
		inv.Cycle = 0
		return true
	}
	return false
}

// RightmostPos gets the rightmost position of an invader, so we don't go off the right hand side of the screen.
func (inv *Invaders) RightmostPos() int {
	rightmostPos := 0
	for _, invader := range inv.Invaders {
		rightmostChar := invader.Y + len(invader.DrawData[0].chars[0]) - 1
		if rightmostChar > rightmostPos {
			rightmostPos = rightmostChar
		}
	}
	return rightmostPos
}

// LeftmostPos gets the leftmost position of an invader, so we don't go off the left hand side of the screen.
func (inv *Invaders) LeftmostPos() int {
	leftmostPos := math.MaxInt
	for _, invader := range inv.Invaders {
		leftmostChar := invader.Y
		if leftmostChar < leftmostPos {
			leftmostPos = leftmostChar
		}
	}
	return leftmostPos
}

// BottommostPos calculates the bottommost grid position which an invader is occupying to see if we have reached the defender's area.
func (inv *Invaders) BottommostPos() int {
	bottommostPos := 0
	for _, invader := range inv.Invaders {
		if invader.status == statusAlive && invader.X > bottommostPos {
			bottommostPos = invader.X + len(invader.DrawData[0].chars[0]) - 1
		}
	}
	return bottommostPos
}

// BottommostPosAtY calculates the bottommost position of the invaders at a certain horizontal position (to determine where the missile should be launched from).
func (inv *Invaders) BottommostPosAtY(y int) int {
	bottommostPos := 0
	for _, invader := range inv.Invaders {
		if invader.Y == y && invader.X > bottommostPos {
			bottommostPos = invader.X + len(invader.DrawData[0].chars[0]) - 1
		}
	}
	return bottommostPos
}

// NextRow move the invaders to the next row down.
func (inv *Invaders) NextRow() {
	for i := range inv.Invaders {
		inv.Invaders[i].X++
	}
}

// NextCol move the invaders to the next column left or right.
func (inv *Invaders) NextCol(step int) {
	for i := range inv.Invaders {
		inv.Invaders[i].Y += step
	}
}

// DecideOnShoot decides on whether a shot should be made by the invaders and which invader it should come from.
// The shoot probability value of the level determines how often a missile launch might happen. Returns nil if no shot made.
func (inv *Invaders) DecideOnShoot() *Missile {
	if inv.ShootProbability <= 0 || rand.IntN(inv.ShootProbability) == 0 {
		colsWithInvaders := make([]int, 0, 12)
		for _, invader := range inv.Invaders {
			if !slices.Contains(colsWithInvaders, invader.Y) {
				colsWithInvaders = append(colsWithInvaders, invader.Y)
			}
		}
		if len(colsWithInvaders) > 0 {
			randomIndex := rand.IntN(len(colsWithInvaders))
			if randomIndex >= 0 && randomIndex < len(colsWithInvaders) {
				missileX := inv.BottommostPosAtY(colsWithInvaders[randomIndex])
				newMissile := NewMissile(false, missileX, colsWithInvaders[randomIndex])
				return &newMissile
			}
		}
	}
	return nil
}

// HitInGrid returns if an invader is hit with the supplied grid position and sets it to dying status.
func (inv *Invaders) HitInGrid(x, y int) bool {
	for _, invader := range inv.Invaders {
		if invader.X == x && invader.Y == y {
			invader.Health--
			if invader.Health <= 0 {
				invader.status = statusDying
				return true
			}
		}
	}
	return false
}

// DrawInGrid draws the invaders in the specified grid. If they are dying then they are drawn as dying and marked for removal.
// Each invader's draw cycle is updated so the next frame of their animation is drawn.
func (inv *Invaders) DrawInGrid(grid Grid) {
	for invader := range slices.Values(inv.Invaders) {
		switch invader.status {
		case statusDying:
			for i, row := range invader.DyingData {
				for j, char := range row {
					grid[invader.X+i][invader.Y+j].Invader = &CellItem[Invader]{
						Item:       invader,
						RenderChar: styles.DeathStyle.Render(char),
					}
				}
			}
			invader.status = statusDead
		default:
			style := invader.Styles[0]
			if invader.Health <= len(invader.Styles) {
				style = invader.Styles[invader.Health-1]
			}
			drawGrid := invader.DrawData[invader.cycle]
			invader.cycle++
			if invader.cycle >= len(invader.DrawData) {
				invader.cycle = 0
			}
			for i, row := range drawGrid.chars {
				for j, char := range row {
					grid[invader.X+i][invader.Y+j].Invader = &CellItem[Invader]{
						Item:       invader,
						RenderChar: style.Render(char),
					}
				}
			}
		}
	}
}
