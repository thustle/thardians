package game

import "thardians/pkg/styles"

// Defender is the ship the player controls.
type Defender struct {
	X, Y      int
	Dying     bool
	DyingData [][]string
}

func NewDefender(x, y int) Defender {
	return Defender{
		X: x,
		Y: y,
		DyingData: [][]string{
			{"\\", "|", "/"},
			{"/", "|", "\\"},
		},
	}
}

// Move moves the defender left (if step < 0) or right (if step > 1) up to max value.
func (d *Defender) Move(step, max int) {
	if step < 0 && d.Y > 0 {
		d.Y--
	} else if step > 0 && d.Y+2 < max {
		d.Y++
	}
}

// IsHit determines whether the specified coordinates (x, y) hit the Defender.
// It returns true if the coordinates match any of the positions the defender is drawn in, and false otherwise.
func (d *Defender) IsHit(x, y int) bool {
	return (x == d.X && y == d.Y+1) ||
		(x == d.X+1 && y == d.Y) ||
		(x == d.X+1 && y == d.Y+1) ||
		(x == d.X+1 && y == d.Y+2)
}

// DrawInGrid draws the defender ship characters in the supplied grid
func (d *Defender) DrawInGrid(grid Grid) {
	if d.Dying {
		for x, row := range d.DyingData {
			for y, item := range row {
				grid[d.X+x][d.Y+y].Defender = &CellItem[Defender]{
					Item:       d,
					RenderChar: styles.DeathStyle.Render(item),
				}
			}
		}
	} else {
		grid[d.X][d.Y+1].Defender = &CellItem[Defender]{
			Item:       d,
			RenderChar: "▲",
		}
		grid[d.X+1][d.Y].Defender = &CellItem[Defender]{
			Item:       d,
			RenderChar: "◢",
		}
		grid[d.X+1][d.Y+1].Defender = &CellItem[Defender]{
			Item:       d,
			RenderChar: "█",
		}
		grid[d.X+1][d.Y+2].Defender = &CellItem[Defender]{
			Item:       d,
			RenderChar: "◣",
		}
	}
}
