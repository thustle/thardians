package game

type CellItem[T any] struct {
	Item       *T
	RenderChar string
}

// Cell represents what can occupy a grid cell of the game. In the case of a collision more than one item may be populated.
// Some collisions are acceptable (e.g. friendly and non-friendly missile), most aren't and will be handled by the collision detection.
type Cell struct {
	Invader  *CellItem[Invader]
	Missile  *CellItem[Missile]
	Shield   *CellItem[Shield]
	Defender *CellItem[Defender]
}

func (c *Cell) Clear() {
	c.Missile = nil
	c.Invader = nil
	c.Shield = nil
	c.Defender = nil
}

// RenderString What to render out of this cell? Missile takes highest priority as that item wins out over others.
// Invader wins out over shield bricks (and removes them). Defender is last :(
func (c *Cell) RenderString() string {
	if c.Missile != nil {
		return c.Missile.RenderChar
	}
	if c.Invader != nil {
		return c.Invader.RenderChar
	}
	if c.Shield != nil {
		return c.Shield.RenderChar
	}
	if c.Defender != nil {
		return c.Defender.RenderChar
	}
	return " "
}
