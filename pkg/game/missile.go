package game

import (
	"github.com/charmbracelet/lipgloss"
	"thardians/pkg/styles"
)

// Missile a missile, either friendly (shot by defender) or not (shot by an invader).
type Missile struct {
	style    lipgloss.Style
	X, Y     int
	Friendly bool
}

func NewMissile(friendly bool, x, y int) Missile {
	return Missile{
		X:        x,
		Y:        y,
		Friendly: friendly,
		style:    getMissileStyle(friendly),
	}
}

func getMissileStyle(friendly bool) lipgloss.Style {
	if friendly {
		return lipgloss.NewStyle()
	} else {
		return styles.InvaderMissile
	}
}

func (m Missile) DrawInGrid(grid [][]Cell) {
	grid[m.X][m.Y].Missile = &CellItem[Missile]{
		Item:       &m,
		RenderChar: m.style.Render("|"),
	}
}

type Missiles []Missile

// DrawInGrid draws all the missiles in the game on the supplied grid.
func (m Missiles) DrawInGrid(grid Grid) {
	for _, missile := range m {
		missile.DrawInGrid(grid)
	}
}

// MoveMissiles moves all the missiles in the game either up (friendly) or down (unfriendly) and removes out of bounds ones.
func MoveMissiles(m Missiles, limit int) Missiles {
	missiles := make([]Missile, 0, len(m))
	for _, missile := range m {
		if missile.Friendly {
			missile.X--
			if missile.X >= 0 {
				missiles = append(missiles, missile)
			}
		} else {
			missile.X++
			if missile.X <= limit {
				missiles = append(missiles, missile)
			}
		}
		missile.Y++
	}
	return missiles
}

func (m Missiles) CountFriendly() int {
	count := 0
	for _, missile := range m {
		if missile.Friendly {
			count++
		}
	}
	return count
}
