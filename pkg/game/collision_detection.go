package game

import "golang.org/x/exp/slices"

type ResultState int

const (
	ResultStateOK ResultState = iota
	ResultStateLevelComplete
	ResultStateDeadRestart
	ResultStateHit
)

type CollisionResult struct {
	Points int
	State  ResultState
}

// collisionDetection is meant to be called in the Tick frame update of the game and will look to see what has hit what.
func collisionDetection(model *Model) CollisionResult {
	result := CollisionResult{
		State: ResultStateOK,
	}

	removeDeadInvaders(model)
	if invaderReachedBottom(model) {
		model.defender.Dying = true
		result.State = ResultStateDeadRestart
		return result
	}
	for i, row := range model.grid {
		for j := range row {
			doInvadersWithShield(model, i, j)
			doMissilesWithShield(model, i, j)
			doMissilesWithInvaders(model, &result, i, j)
			if len(model.invaders.Invaders) <= 0 {
				result.State = ResultStateLevelComplete
				return result
			}
			if doMissilesWithDefender(model, i, j) {
				result.State = ResultStateHit
				return result
			}
		}
	}
	return result
}

func removeDeadInvaders(model *Model) {
	model.invaders.Invaders = slices.DeleteFunc(model.invaders.Invaders, func(invader *Invader) bool {
		return invader.status == statusDead
	})
	left := len(model.invaders.Invaders) / 9 // Speed up the invaders by reducing the updateEvery cycle skip count based on number of invaders left
	model.invaders.UpdateEvery = left
}

func invaderReachedBottom(model *Model) bool {
	return model.invaders.BottommostPos() >= model.defender.X
}

func removeMissile(model *Model, x, y int) {
	model.missiles = slices.DeleteFunc(model.missiles, func(m Missile) bool {
		return m.X == x && m.Y == y
	})
}

func doMissilesWithInvaders(model *Model, result *CollisionResult, i, j int) {
	cell := model.grid[i][j]
	if cell.Missile != nil && cell.Missile.Item.Friendly && cell.Invader != nil {
		cell.Invader.Item.Health--
		if cell.Invader.Item.Health <= 0 && cell.Invader.Item.status == statusAlive {
			cell.Invader.Item.status = statusDying
			result.Points += cell.Invader.Item.Points
		}
		removeMissile(model, i, j)
	}
}

func removeShield(model *Model, x, y int) {
	model.shields = slices.DeleteFunc(model.shields, func(s *Shield) bool {
		return s.X == x && s.Y == y
	})
}

func doInvadersWithShield(model *Model, i, j int) {
	cell := model.grid[i][j]
	if cell.Invader != nil && cell.Shield != nil {
		removeShield(model, i, j)
	}
}

func doMissilesWithShield(model *Model, i, j int) {
	cell := model.grid[i][j]
	if cell.Missile != nil && cell.Shield != nil {
		removeShield(model, i, j)
		removeMissile(model, i, j)
	}
}

func doMissilesWithDefender(model *Model, i, j int) bool {
	cell := model.grid[i][j]
	if cell.Missile != nil && cell.Defender != nil && cell.Defender.Item.IsHit(i, j) {
		cell.Defender.Item.Dying = true
		removeMissile(model, i, j)
		return true
	}
	return false
}
