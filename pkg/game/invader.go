package game

import "github.com/charmbracelet/lipgloss"

type InvaderDrawCycle struct {
	chars [][]string
}

type status int

const (
	statusAlive status = iota
	statusDying
	statusDead
)

// Invader represents an 'alien'. There are currently 2 types with different sizes and looks (Pawn and Ship).
type Invader struct {
	Styles    []lipgloss.Style
	X, Y      int
	Health    int
	cycle     int
	status    status
	DrawData  []InvaderDrawCycle
	DyingData [][]string
	Points    int
}

func NewPawn(x, y, health int) *Invader {
	return &Invader{
		Styles: []lipgloss.Style{lipgloss.NewStyle().Foreground(lipgloss.Color("215")), lipgloss.NewStyle().Foreground(lipgloss.Color("63"))},
		X:      x,
		Y:      y,
		Health: health,
		DrawData: []InvaderDrawCycle{
			{chars: [][]string{
				{"◢", "◣"},
			}},
			{chars: [][]string{
				{"◢", "◣"},
			}},
			{chars: [][]string{
				{"◤", "◥"},
			}},
			{chars: [][]string{
				{"◤", "◥"},
			}},
		},
		DyingData: [][]string{
			{">", "<"},
		},
		status: statusAlive,
		Points: 10,
	}
}

func NewShip(x, y, health int) *Invader {
	return &Invader{
		Styles: []lipgloss.Style{lipgloss.NewStyle().Foreground(lipgloss.Color("173")), lipgloss.NewStyle().Foreground(lipgloss.Color("30"))},
		X:      x,
		Y:      y,
		Health: health,
		DrawData: []InvaderDrawCycle{
			{chars: [][]string{
				{"◒", "◒"},
				{"/", "\\"},
			}},
			{chars: [][]string{
				{"◒", "◒"},
				{"/", "\\"},
			}},
			{chars: [][]string{
				{"◐", "◑"},
				{"\\", "/"},
			}},
			{chars: [][]string{
				{"◐", "◑"},
				{"\\", "/"},
			}},
			{chars: [][]string{
				{"◓", "◓"},
				{"/", "\\"},
			}},
			{chars: [][]string{
				{"◓", "◓"},
				{"/", "\\"},
			}},
			{chars: [][]string{
				{"◑", "◐"},
				{"\\", "/"},
			}},
			{chars: [][]string{
				{"◑", "◐"},
				{"\\", "/"},
			}},
		},
		DyingData: [][]string{
			{"\\", "/"},
			{"/", "\\"},
		},
		status: statusAlive,
		Points: 20,
	}
}
