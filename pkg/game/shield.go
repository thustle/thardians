package game

import (
	"github.com/charmbracelet/lipgloss"
)

// Shield represents a brick. A full shield will be a slice of many of these.
type Shield struct {
	style lipgloss.Style
	X, Y  int
}

var shieldMap = [][]string{
	{" ", " ", "█", "█", "█", " ", " "},
	{" ", "█", "█", "█", "█", "█", " "},
	{"█", "█", "█", " ", "█", "█", "█"},
}

// NewShield returns a list of Shield bricks which make up a shield
func NewShield(x, y int) []*Shield {
	elements := make([]*Shield, 0, 14)
	for i, row := range shieldMap {
		for j, char := range row {
			if char != " " {
				elements = append(elements, &Shield{
					X: x + i,
					Y: y + j,
				})
			}
		}
	}
	return elements
}

func (s Shield) DrawInGrid(grid [][]Cell) {
	grid[s.X][s.Y].Shield = &CellItem[Shield]{
		Item:       &s,
		RenderChar: s.style.Render("█"),
	}
}

type Shields []*Shield

// SetupShields creates the 3 structures of shield bricks for each level.
func SetupShields() Shields {
	shield1Elements := NewShield(16, 5)
	shield2Elements := NewShield(16, 37)
	shield3Elements := NewShield(16, 66)

	shields := make([]*Shield, 0)
	shields = append(shields, shield1Elements...)
	shields = append(shields, shield2Elements...)
	shields = append(shields, shield3Elements...)
	return shields
}

func (s Shields) DrawInGrid(grid Grid) {
	for _, shield := range s {
		shield.DrawInGrid(grid)
	}
}
