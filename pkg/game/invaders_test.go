package game

import (
	"github.com/charmbracelet/lipgloss"
	"testing"
)

func TestSetupInvaders(t *testing.T) {
	tests := []struct {
		name string
		lvl  Level
	}{
		{
			name: "TestWithDefaultLevel",
			lvl:  GetLevel(1),
		},
		{
			name: "TestWithSecondLevel",
			lvl:  GetLevel(2),
		},
		{
			name: "TestWithThirdLevel",
			lvl:  GetLevel(3),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oldInvaders := SetupInvaders(tt.lvl)
			if len(oldInvaders.Invaders) != 48 {
				t.Errorf("SetupInvaders() = %v, want %v", len(oldInvaders.Invaders), 48)
			}

			for _, invader := range oldInvaders.Invaders {
				if invader.Health != tt.lvl.ShipInitialHealth && invader.Health != tt.lvl.PawnInitialHealth {
					t.Errorf("Health of invader doesn't match expected values = (either %v or %v), got %v", tt.lvl.ShipInitialHealth, tt.lvl.PawnInitialHealth, invader.Health)
				}
			}

			if oldInvaders.UpdateEvery != tt.lvl.InvadersUpdateEvery {
				t.Errorf("UpdateEvery of invaders doesn't match expected = %v, got %v", tt.lvl.InvadersUpdateEvery, oldInvaders.UpdateEvery)
			}

			if oldInvaders.MaxUpdateEvery != tt.lvl.InvadersUpdateEvery {
				t.Errorf("MaxUpdateEvery of invaders doesn't match expected = %v, got %v", tt.lvl.InvadersUpdateEvery, oldInvaders.MaxUpdateEvery)
			}

			if oldInvaders.ShootProbability != tt.lvl.InvaderShootProbability {
				t.Errorf("ShootProbability of invaders doesn't match expected = %v, got %v", tt.lvl.InvaderShootProbability, oldInvaders.ShootProbability)
			}
		})
	}
}

func TestCycleTick(t *testing.T) {
	tests := []struct {
		name                               string
		cycle, updateEvery, maxUpdateEvery int
		expected                           bool
	}{
		{"Start of cycle", 0, 5, 5, false},
		{"End of cycle", 3, 4, 5, true},
		{"Hit max", 3, 5, 4, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			invaders := Invaders{
				Cycle:          tt.cycle,
				UpdateEvery:    tt.updateEvery,
				MaxUpdateEvery: tt.maxUpdateEvery,
			}
			result := invaders.Tick()
			if result != tt.expected {
				t.Errorf("Tick() result doesn't match expected = %v, got %v", tt.expected, result)
			}
		})
	}
}

func TestInvadersRightMostPos(t *testing.T) {
	invaders := SetupInvaders(GetLevel(1))
	expected := 45
	result := invaders.RightmostPos()
	if result != expected {
		t.Errorf("RightmostPos() result doesn't match expected = %v, got %v", expected, result)
	}
}

func TestInvadersLeftMostPos(t *testing.T) {
	invaders := SetupInvaders(GetLevel(1))
	expected := 0
	result := invaders.LeftmostPos()
	if result != expected {
		t.Errorf("LeftmostPos() result doesn't match expected = %v, got %v", expected, result)
	}
}

func TestInvadersBottomMostPos(t *testing.T) {
	invaders := SetupInvaders(GetLevel(1))
	expected := 9
	result := invaders.BottommostPos()
	if result != expected {
		t.Errorf("BottommostPos() result doesn't match expected = %v, got %v", expected, result)
	}
}

func TestInvadersBottomMostPosAtY(t *testing.T) {
	invaders := SetupInvaders(GetLevel(1))
	invadersMinusBottomLeft := make([]*Invader, len(invaders.Invaders)-1)
	i := 0
	for _, invader := range invaders.Invaders {
		if !(invader.X == 8 && invader.Y == 0) {
			invadersMinusBottomLeft[i] = invader
			i++
		}
	}
	invaders.Invaders = invadersMinusBottomLeft
	expected := 7
	result := invaders.BottommostPosAtY(0)
	if result != expected {
		t.Errorf("BottommostPos() result doesn't match expected = %v, got %v", expected, result)
	}
}

func TestNextRow(t *testing.T) {
	invaders := SetupInvaders(GetLevel(1))
	invadersMoved := SetupInvaders(GetLevel(1))
	invadersMoved.NextRow()

	for i := 0; i < len(invadersMoved.Invaders); i++ {
		if invaders.Invaders[i].X+1 != invadersMoved.Invaders[i].X {
			t.Errorf("NextRow(): invader %d did not move to the next row", i)
		}
	}
}

func TestNextCol(t *testing.T) {
	tests := []struct {
		name     string
		initialY int
		step     int
		wantY    int
	}{
		{
			name:     "Right Step",
			initialY: 5,
			step:     1,
			wantY:    6,
		},
		{
			name:     "Left Step",
			initialY: 5,
			step:     -1,
			wantY:    4,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			invaders := Invaders{
				Invaders: []*Invader{
					{
						Y: tt.initialY,
					},
				},
			}
			invaders.NextCol(tt.step)
			if got := invaders.Invaders[0].Y; got != tt.wantY {
				t.Errorf("NextCol() = %v, want %v", got, tt.wantY)
			}
		})
	}
}

func TestHitInGrid(t *testing.T) {
	tests := []struct {
		name           string
		invaders       []*Invader
		x              int
		y              int
		out            bool
		expectedStatus status
	}{
		{
			"Hit invader, alive after hit",
			[]*Invader{{X: 1, Y: 2, Health: 2, status: statusAlive}},
			1,
			2,
			false,
			statusAlive,
		},
		{
			"Hit invader, dead after hit",
			[]*Invader{{X: 1, Y: 2, Health: 1, status: statusAlive}},
			1,
			2,
			true,
			statusDying,
		},
		{
			"No invader at position",
			[]*Invader{{X: 3, Y: 4, Health: 2, status: statusAlive}},
			1,
			2,
			false,
			statusAlive,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			inv := &Invaders{
				Invaders: test.invaders,
			}

			got := inv.HitInGrid(test.x, test.y)
			if got != test.out {
				t.Errorf("got %v, want %v", got, test.out)
				return
			}

			// Check that invader health or status has been adjusted correctly
			if got != test.out {
				t.Errorf("HitInGrid() expected %v got %v", test.out, got)
			}
			if inv.Invaders[0].status != test.expectedStatus {
				t.Errorf("HitInGrid() expected status %v got %v", test.expectedStatus, inv.Invaders[0].status)
			}
		})
	}
}

func TestDecideOnShoot(t *testing.T) {
	invader1 := &Invader{Y: 5}
	invader2 := &Invader{Y: 7}
	invader3 := &Invader{Y: 5}

	tests := []struct {
		name        string
		probability int
		wantNil     bool
	}{
		{
			name:        "probability check true",
			probability: 0,
			wantNil:     false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			invaders := &Invaders{
				Invaders:         []*Invader{invader1, invader2, invader3},
				ShootProbability: tc.probability,
			}

			got := invaders.DecideOnShoot()

			if (got == nil) != tc.wantNil {
				t.Errorf("DecideOnShoot() = %v, wantNil %v", got, tc.wantNil)
			}

			expectedMissileX := invaders.BottommostPosAtY(got.Y)
			if got.X != expectedMissileX || got.Friendly != false {
				t.Errorf("DecideOnShoot() = %v, want missile with X = %v, Y = %v, Friendly = %v", got, expectedMissileX, got.Y, false)
			}
		})
	}
}

func TestInvaders_DrawInGrid(t *testing.T) {
	invader := Invader{
		status: statusAlive,
		X:      0,
		Y:      1,
		cycle:  0,
		Health: 1,
		Styles: []lipgloss.Style{lipgloss.NewStyle()},
		DrawData: []InvaderDrawCycle{
			{
				chars: [][]string{{"X", "X"}},
			},
		},
	}
	dyingInvader := Invader{
		status:    statusDying,
		X:         0,
		Y:         1,
		DyingData: [][]string{{"-", "-"}},
	}
	tests := []struct {
		name     string
		invaders Invaders
		expected Grid
	}{
		{
			name: "Testing single Invader",
			invaders: Invaders{
				Invaders: []*Invader{
					&invader,
				},
			},
			expected: func() Grid {
				grid := blankGridWithXY(1, 3)
				grid[0][0].Invader = nil
				grid[0][1].Invader = &CellItem[Invader]{Item: &dyingInvader, RenderChar: "X"}
				grid[0][2].Invader = &CellItem[Invader]{Item: &dyingInvader, RenderChar: "X"}
				return grid
			}(),
		},
		{
			name: "Testing dying Invader",
			invaders: Invaders{
				Invaders: []*Invader{
					&dyingInvader,
				},
			},
			expected: func() Grid {
				grid := blankGridWithXY(1, 3)
				grid[0][0].Invader = nil
				grid[0][1].Invader = &CellItem[Invader]{Item: &dyingInvader, RenderChar: "-"}
				grid[0][2].Invader = &CellItem[Invader]{Item: &dyingInvader, RenderChar: "-"}
				return grid
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			grid := blankGridWithXY(1, 3)
			tt.invaders.DrawInGrid(grid)
			if !compareGrids(grid, tt.expected) {
				t.Errorf("Got %v, want %v", grid, tt.expected)
			}
		})
	}
}
