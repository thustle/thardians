package game

import (
	"testing"
)

func TestDefenderMove(t *testing.T) {
	type args struct {
		step int
		max  int
	}
	tests := []struct {
		name string
		d    Defender
		args args
		want int
	}{
		{
			name: "NoMove",
			d:    NewDefender(1, 2),
			args: args{step: 0, max: 5},
			want: 2,
		},
		{
			name: "StepLeft",
			d:    NewDefender(1, 2),
			args: args{step: -1, max: 5},
			want: 1,
		},
		{
			name: "StepRight",
			d:    NewDefender(1, 2),
			args: args{step: 1, max: 5},
			want: 3,
		},
		{
			name: "StepRightAtMax",
			d:    NewDefender(1, 3),
			args: args{step: 1, max: 5},
			want: 3,
		},
		{
			name: "StepLeftAtZero",
			d:    NewDefender(1, 0),
			args: args{step: -1, max: 5},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.d.Move(tt.args.step, tt.args.max)
			if tt.d.Y != tt.want {
				t.Errorf("Defender.Move() = %v, want %v", tt.d.Y, tt.want)
			}
		})
	}
}

func TestDefenderIsHit(t *testing.T) {
	type test struct {
		name string
		d    Defender
		x, y int
		want bool
	}

	tests := []test{
		{
			name: "Case Direct Hit on nose",
			d:    NewDefender(5, 7),
			x:    5,
			y:    8,
			want: true,
		},
		{
			name: "Case Left Wing Hit",
			d:    NewDefender(5, 7),
			x:    6,
			y:    7,
			want: true,
		},
		{
			name: "Case Centre Hit",
			d:    NewDefender(5, 7),
			x:    6,
			y:    8,
			want: true,
		},
		{
			name: "Case Right Wing Hit",
			d:    NewDefender(5, 7),
			x:    6,
			y:    9,
			want: true,
		},
		{
			name: "Case Miss Left",
			d:    NewDefender(5, 7),
			x:    5,
			y:    7,
			want: false,
		},
		{
			name: "Case Miss Right",
			d:    NewDefender(5, 7),
			x:    5,
			y:    9,
			want: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			if got := tc.d.IsHit(tc.x, tc.y); got != tc.want {
				t.Errorf("Defender.IsHit() = %v, want %v", got, tc.want)
			}
		})
	}
}

func TestDefenderDrawInGrid(t *testing.T) {
	grid := blankGrid()

	normalDefender := NewDefender(0, 0)
	dyingDefender := NewDefender(0, 0)
	dyingDefender.Dying = true

	testCases := []struct {
		name     string
		defender Defender
		expected Grid
	}{
		{
			name:     "NormalDefender",
			defender: normalDefender,
			expected: func() Grid {
				grid := blankGrid()
				grid[0][0].Defender = nil
				grid[0][1].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "▲"}
				grid[0][2].Defender = nil
				grid[1][0].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "◢"}
				grid[1][1].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "█"}
				grid[1][2].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "◣"}
				return grid
			}(),
		},
		{
			name:     "DyingDefender",
			defender: dyingDefender,
			expected: func() Grid {
				grid := blankGrid()
				grid[0][0].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "\\"}
				grid[0][1].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "|"}
				grid[0][2].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "/"}
				grid[1][0].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "/"}
				grid[1][1].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "|"}
				grid[1][2].Defender = &CellItem[Defender]{Item: &normalDefender, RenderChar: "\\"}
				return grid
			}(),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.defender.DrawInGrid(grid)
			if !compareGrids(grid, tc.expected) {
				t.Errorf("Got %v, want %v", grid, tc.expected)
			}
		})
	}
}
