package styles

import "github.com/charmbracelet/lipgloss"

const MaxWidth = 80
const MaxHeight = 25

var BorderStyle = lipgloss.NewStyle().
	Padding(0, 0).
	Border(lipgloss.RoundedBorder()).
	BorderForeground(lipgloss.Color("62"))

var TitleStyle = lipgloss.NewStyle().
	Background(lipgloss.Color("62")).
	Foreground(lipgloss.Color("230")).
	Padding(0, 2).
	MarginLeft(15)

var InvaderMissile = lipgloss.NewStyle().Foreground(lipgloss.Color("70"))

var DeathStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("91"))

var ScoreStyle = lipgloss.NewStyle().
	Background(lipgloss.Color("11")).
	Foreground(lipgloss.Color("0")).
	Padding(0, 1)

var TitleScreenInstructions = lipgloss.NewStyle().
	Background(lipgloss.Color("11")).
	Foreground(lipgloss.Color("0")).
	Padding(0, 1).
	MarginLeft(20)
