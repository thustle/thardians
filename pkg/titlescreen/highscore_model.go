package titlescreen

import (
	"fmt"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"thardians/pkg/styles"
)

// highscoreModel bubble tea model to get the name of the highscorer and add it to the highscores.
type highscoreModel struct {
	backModel     tea.Model
	style         lipgloss.Style
	width, height int
	lastScore     int
	input         textinput.Model
	err           string
}

func newHighscoreModel(backModel tea.Model, style lipgloss.Style, score, width, height int) highscoreModel {
	model := highscoreModel{
		backModel: backModel,
		style:     style,
		width:     width,
		height:    height,
		lastScore: score,
		input:     textinput.New(),
	}
	model.input.CharLimit = 20
	model.input.Focus()
	return model
}

func (m highscoreModel) hasPlayableSize() bool {
	return m.height >= 25 && m.width >= 80
}

// Init is run once when the program starts
func (m highscoreModel) Init() tea.Cmd {
	return nil
}

func (m highscoreModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	commands := make([]tea.Cmd, 0)
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = min(80, msg.Width)
		m.height = min(25, msg.Height)
	case tea.KeyMsg:
		switch msg.String() {
		case "esc": // Player doesn't want to be entered in the hall of fame.
			return m.backModel, cycleScoresCmd()
		case "enter":
			if m.input.Value() != "" {
				_, err := AddScore(m.lastScore, m.input.Value())
				if err != nil {
					m.err = err.Error()
				} else {
					return m.backModel, tea.Batch(LoadScoreCmd(), cycleScoresCmd())
				}
			}
		default:
			var cmd tea.Cmd
			m.input, cmd = m.input.Update(msg)
			commands = append(commands, cmd)
		}
	}
	return m, tea.Batch(commands...)
}

func (m highscoreModel) View() string {
	if !m.hasPlayableSize() {
		return styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
			"Please increase your term size to at least 80x25",
		)
	}
	topPanel := lipgloss.JoinVertical(
		lipgloss.Top,
		m.style.Render(titleText),
		styles.TitleStyle.MarginTop(1).MarginBottom(2).MarginLeft(4).Render(fmt.Sprintf("You made the hall of fame with %d points. Please enter your name.", m.lastScore)),
	)

	input := styles.BorderStyle.Width(m.width - 4).Render(m.input.View())
	body := lipgloss.JoinVertical(lipgloss.Top, topPanel, input, m.style.Render(m.err))
	panel := styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
		body,
	)
	return panel
}
