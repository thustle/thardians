package titlescreen

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sort"
	"sync"
)

type Highscore struct {
	Score int    `json:"score"`
	Name  string `json:"name"`
}

var highscoreMutex sync.Mutex
var highscoreCache []Highscore
var maxHighscores = 10

func isHighscore(points int) bool {
	if points <= 0 {
		return false
	}
	if len(highscoreCache) < maxHighscores {
		return true
	}
	for _, hs := range highscoreCache {
		if points > hs.Score {
			return true
		}
	}
	return false
}

func getHighscoreFilePath() (string, error) {
	var dir string
	userDir, err := os.UserConfigDir()
	dir = fmt.Sprintf("%s%c%s", userDir, os.PathSeparator, "thardians")
	_, err = os.Stat(dir)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(dir, os.ModePerm)
		}
		if err != nil {
			return dir, err
		}
	}
	highscoreFile := fmt.Sprintf("%s%c%s", dir, os.PathSeparator, "highscores.json")
	return highscoreFile, nil
}

func AddScore(score int, name string) ([]Highscore, error) {
	existingScores, err := GetScores()
	existingScores = append(existingScores, Highscore{Score: score, Name: name})
	sort.Slice(existingScores, func(i, j int) bool {
		return existingScores[i].Score > existingScores[j].Score
	})
	if len(existingScores) >= maxHighscores {
		existingScores = existingScores[:maxHighscores]
	}

	highscoreMutex.Lock()
	defer highscoreMutex.Unlock()
	filePath, err := getHighscoreFilePath()
	if err != nil {
		return nil, err
	}
	file, err := os.Create(filePath)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	err = saveScores(file, existingScores)
	highscoreCache = existingScores
	return existingScores, err
}

func GetScores() ([]Highscore, error) {
	highscoreMutex.Lock()
	defer highscoreMutex.Unlock()

	filePath, err := getHighscoreFilePath()
	if err != nil {
		return nil, err
	}
	file, err := os.Open(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			return []Highscore{}, nil
		} else {
			return nil, err
		}
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)
	scores, err := loadScores(file)
	highscoreCache = scores
	return scores, err
}

func loadScores(reader io.Reader) ([]Highscore, error) {
	scores := make([]Highscore, 0)
	decoder := json.NewDecoder(reader)
	err := decoder.Decode(&scores)
	return scores, err
}

func saveScores(writer io.Writer, scores []Highscore) error {
	decoder := json.NewEncoder(writer)
	err := decoder.Encode(scores)
	return err
}
