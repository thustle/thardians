package titlescreen

import (
	"fmt"
	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"strings"
	"thardians/pkg/game"
	"thardians/pkg/styles"
	"time"
)

// Model the model of the title screen
type Model struct {
	style          lipgloss.Style
	width, height  int
	lastScore      int
	highscoreItems list.Model
	errorMsg       string
	useHighscores  bool
}

// LoadScores command for Update() to load the highscores.
type LoadScores struct{}

func LoadScoreCmd() tea.Cmd {
	return tea.Tick(time.Millisecond*0, func(t time.Time) tea.Msg {
		return LoadScores{}
	})
}

// cycleScores command for Update() to cycle through the highscores list.
type cycleScores struct{}

func cycleScoresCmd() tea.Cmd {
	return tea.Tick(time.Second*5, func(t time.Time) tea.Msg {
		return cycleScores{}
	})
}

func NewModel(useHighscores bool) Model {
	return Model{
		style:          lipgloss.NewStyle(),
		highscoreItems: createHighscoreItemsList(),
		useHighscores:  useHighscores,
	}
}

func NewModelWithRenderer(renderer *lipgloss.Renderer, useHighscores bool) Model {
	return Model{
		style:          renderer.NewStyle(),
		highscoreItems: createHighscoreItemsList(),
		useHighscores:  useHighscores,
	}
}

func (m Model) hasPlayableSize() bool {
	return m.height >= 25 && m.width >= 80
}

// Init is run once when the program starts
func (m Model) Init() tea.Cmd {
	return tea.Batch(LoadScoreCmd(), cycleScoresCmd())
}

// Update handles game state changes.
func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	commands := make([]tea.Cmd, 0)
	switch msg := msg.(type) {
	case tea.WindowSizeMsg: // Record the current size of the terminal.
		m.width = min(80, msg.Width)
		m.height = min(25, msg.Height)
	case LoadScores: // Load the highscores.
		if !m.useHighscores {
			return m, nil
		}
		scores, err := GetScores()
		if err != nil {
			m.errorMsg = err.Error()
		} else {
			updateHighscoreList(&m, scores)
		}
	case cycleScores: // Cycle through the pages of highscores.
		if !m.useHighscores {
			return m, nil
		}
		if m.highscoreItems.Paginator.OnLastPage() {
			scores, _ := GetScores()
			m.highscoreItems.Paginator.Page = 0
			cmd := updateHighscoreList(&m, scores)
			commands = append(commands, cmd, cycleScoresCmd())
		} else {
			m.highscoreItems.Paginator.NextPage()
			_, cmd := m.highscoreItems.Update(nil)
			commands = append(commands, cmd, cycleScoresCmd())
		}
	case game.EndGame: // The game has ended. If score is a highscore then move to 'enter highscore name' screen.
		m.lastScore = msg.Points
		if m.useHighscores && isHighscore(msg.Points) {
			return newHighscoreModel(m, m.style, m.lastScore, m.width, m.height), nil
		} else {
			commands = append(commands, cycleScoresCmd())
		}
	case tea.KeyMsg: // Handle player input (start new game, quit, manually cycle through highscores).
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "1":
			if m.hasPlayableSize() {
				newGame := game.NewModel(m, m.style, m.width, m.height)
				return newGame, game.StartGame()
			}
		case "j", "down":
			m.highscoreItems.Paginator.NextPage()
			_, cmd := m.highscoreItems.Update(nil)
			commands = append(commands, cmd)
		case "k", "up":
			m.highscoreItems.Paginator.PrevPage()
			_, cmd := m.highscoreItems.Update(nil)
			commands = append(commands, cmd)
		}
	}
	return m, tea.Batch(commands...)
}

// View displays the current game state
func (m Model) View() string {
	if !m.hasPlayableSize() {
		return styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
			"Please increase your term size to at least 80x25",
		)
	}
	topPanel := m.style.Render(titleText)
	columns := []table.Column{
		{Title: "", Width: 15},
		{Title: "", Width: 40},
	}
	commandsTable := table.New(
		table.WithColumns(columns),
		table.WithRows(createCommandRows()),
		table.WithFocused(false),
		table.WithStyles(table.Styles{
			Header:   m.style.Foreground(lipgloss.Color("11")),
			Cell:     m.style.Foreground(lipgloss.Color("11")),
			Selected: m.style.Foreground(lipgloss.Color("11")),
		}),
		table.WithHeight(5),
	)

	lastScoreText := ""
	if m.lastScore > 0 {
		lastScoreText = styles.ScoreStyle.Render(fmt.Sprintf("Last Score: %d", m.lastScore))
	}
	middlePanel := lipgloss.JoinVertical(lipgloss.Top,
		styles.TitleScreenInstructions.Render("Press 1 to start game, q to quit."),
		m.style.MarginLeft(15).Render(commandsTable.View()),
		highscoresView(m),
		m.style.Render(lastScoreText),
	)
	body := lipgloss.JoinVertical(lipgloss.Top, topPanel, middlePanel)
	panel := styles.BorderStyle.Width(m.width - 2).Height(m.height - 2).Render(
		body,
	)
	return panel
}

const commandsContent = `Cursor ← → = Move Ship left and right
Space bar = Fire missile
p = Pause game
<ESC> = Quit game and come back to menu`

func createCommandRows() []table.Row {
	lines := strings.Split(commandsContent, "\n")
	rows := make([]table.Row, len(lines))
	for i, line := range lines {
		cols := strings.Split(line, " = ")
		rows[i] = table.Row{cols[0], cols[1]}
	}
	return rows
}

func renderErrorMessage(model Model) string {
	if model.errorMsg != "" {
		return model.style.MarginTop(5).MarginLeft(0).Render(fmt.Sprintf("Problem obtaining highscores: %s", model.errorMsg))
	} else {
		return model.style.MarginTop(7).Render("")
	}
}

func highscoresView(m Model) string {
	if len(m.highscoreItems.Items()) == 0 {
		return renderErrorMessage(m)
	}
	return m.style.MarginTop(1).MarginLeft(18).Width(40).Height(4).Render(m.highscoreItems.View())
}

func createHighscoreItemsList() list.Model {
	items := list.New([]list.Item{}, itemDelegate{}, 20, 7)
	items.SetShowHelp(false)
	items.SetShowPagination(false)
	items.SetShowStatusBar(false)
	items.SetShowFilter(false)
	items.Title = "Hall of Fame"
	return items
}

func updateHighscoreList(m *Model, scores []Highscore) tea.Cmd {
	items := make([]list.Item, len(scores))
	for i, obj := range scores {
		items[i] = NewHighscoreItem(obj)
	}
	return m.highscoreItems.SetItems(items)
}
