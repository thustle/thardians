package titlescreen

import (
	"fmt"
	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"io"
)

var (
	itemStyle = lipgloss.NewStyle().PaddingLeft(1).Foreground(lipgloss.Color("174"))
)

type HighscoreItem struct {
	name  string
	score int
}

func NewHighscoreItem(highscore Highscore) HighscoreItem {
	return HighscoreItem{
		name:  highscore.Name,
		score: highscore.Score,
	}
}

func (t HighscoreItem) FilterValue() string {
	return t.name
}

func (t HighscoreItem) Title() string {
	return t.name
}

type itemDelegate struct{}

func (d itemDelegate) Height() int                             { return 1 }
func (d itemDelegate) Spacing() int                            { return 0 }
func (d itemDelegate) Update(_ tea.Msg, _ *list.Model) tea.Cmd { return nil }
func (d itemDelegate) Render(w io.Writer, _ list.Model, index int, listItem list.Item) {
	i, ok := listItem.(HighscoreItem)
	if !ok {
		return
	}
	str := fmt.Sprintf("%2d. %-25s %4d", index+1, i.Title(), i.score)

	fn := itemStyle.Render
	_, err := fmt.Fprint(w, fn(str))
	if err != nil {
		return
	}
}
