# Thardians!

A space invaders game you can play in the terminal, using the Bubble Tea terminal UI. 

You can build the app to run in one of two ways:
1. Build the app to run as a regular terminal application:
```shell
make build
```
2. Build the SSH version and host the game to anyone who connects via SSH
on a specified port (default 1337). When played over SSH then players share the 
same high-score table.
```shell
make build-ssh
```

Why Thardians? I think it was because these games make me think back to 
old 8-bit games I used to play on the Acorn Electron, such as Arcadians, and Elite (with the Thargoids).


## Screenshots
Demo
![Game screen layout (iTerm2)](readme/demo.gif)

Game on iTerm2
![Game screen layout (iTerm2)](readme/screenshot1.png)

For a full retro feel. Try it in cool-retro-term
![Game screen cool-retro-term](readme/screenshot3.png)

Title screen on iTerm2
![Game screen layout (iTerm2)](readme/screenshot2.png)

## Milestones
- [x] Moving band of invaders
- [X] Ship movement
- [X] Invader animation
- [X] Shields
- [X] Missiles from defender
- [X] Invader speed-up
- [X] Collision detection of missiles on invaders
- [X] Title Screen
- [X] Pause Game
- [X] Collision detection of missiles on shields
- [X] Collision detection of invaders on shields
- [X] Missiles from invaders
- [X] Collision detection of missiles on defender
- [X] Start/End game scenarios
- [X] Manage lives
- [X] Levels
- [X] High-scores
- [X] High-score name entry
- [X] Run over ssh with shared high-score board.

## License
[MIT](LICENSE)
